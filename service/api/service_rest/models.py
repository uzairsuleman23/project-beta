from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
  import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
  vin = models.CharField(max_length=17, unique=True)
  color = models.CharField(max_length=50)
  year = models.PositiveSmallIntegerField(blank=False)
  model_name = models.CharField(max_length=100)

  def __str__(self):
    return f"{self.year} {self.model_name} {self.color}"

  class Meta:
      verbose_name_plural = "Automobile VOs"


class Technician(models.Model):
    technician_name = models.CharField(max_length=255)
    employee_number = models.CharField(max_length=255, unique=True)


    def __str__(self):
        return self.technician_name

    def get_api_url(self):
        return reverse("api_show_tech", kwargs={"pk": self.pk})

class Appointment(models.Model):
    customer_name = models.CharField(max_length=255)
    date_time = models.DateTimeField(null=True, auto_now_add=False)
    reason = models.CharField(max_length=255)
    is_completed = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )


    def __str__(self):
        return self.vin

    class Meta:
        ordering = ['-date_time']
