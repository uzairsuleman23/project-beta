from django.urls import path
from .views import api_AutomobileVO, api_sales_person, api_potential_customer, api_sold_automobile, api_sales_person_detail

urlpatterns = [
    path("sales/automobileVO/", api_AutomobileVO, name="api_automobileVO"),
    path("sales/salesperson/", api_sales_person, name="api_sales_person"),
    path("sales/salesperson/<str:employee_number>/", api_sales_person_detail, name="api_sales_person_detail"),
    path("sales/potentialcustomer/", api_potential_customer, name="api_potential_customer"),
    path("sales/soldautomobiles/", api_sold_automobile, name="api_sold_automobiles"),
    path("sales/soldautomobiles/<str:vin>/", api_sold_automobile, name="api_sold_automobile"),
]
