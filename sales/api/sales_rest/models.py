from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model_name = models.CharField(max_length=100)
    manufacturer_name = models.CharField(max_length=100)
    is_sold = models.BooleanField(default=False)

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"employee_nubmer": self.employee_number})

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address1 = models.CharField(max_length=128)
    address2 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=128)
    state = models.CharField(max_length=128)
    zip_code = models.CharField(max_length=5)
    phone_number = models.CharField(max_length=10)

class SoldAutomobile(models.Model):
    sell_price = models.PositiveIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="auto_VO",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sold_automobiles",
        on_delete=models.PROTECT,
    )
    potential_customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sold_autos",
        on_delete=models.PROTECT,
    )
    def get_api_url(self):
        return reverse("api_sold_automobile", kwargs={"vin":self.vin})
