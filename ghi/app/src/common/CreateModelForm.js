import React, {useState} from 'react'

function NewModelForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = modelName
        data.picture_url = pictureUrl
        data.manufacturer_id = manufacturerName
        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setModelName('')
            setPictureUrl('')
            setManufacturerName('')
            props.fetchModels()
        }
    }

    const [modelName, setModelName] = useState("")
    const [pictureUrl, setPictureUrl] = useState("")
    const [manufacturerName, setManufacturerName] = useState("")


    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleManufacturerNameChange = (event) => {
        const value = event.target.value
        setManufacturerName(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={modelName} onChange={handleModelNameChange} required type="text" name="model_name" id="model_name" className="form-control" />
                            <label>Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureUrl} onChange={handlePictureUrlChange} required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label>Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={manufacturerName} onChange={handleManufacturerNameChange} required name="manufacturer_name" id="manufacturer_name" className="form-select">
                                <option value="">Select a manufacturer</option>
                                {props.manufacturers.map(manufacturer => {
                                    return (
                                        <option value={manufacturer.id} key={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewModelForm
