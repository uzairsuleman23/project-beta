import React, {useEffect, useState} from "react"

function NewAutoToInventory(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model
        const url = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setColor("")
            setYear("")
            setVin("")
            setModel("")
            props.fetchAutos()
        }
    }

    const [color, setColor] = useState("")
    const [year, setYear] = useState("")
    const [vin, setVin] = useState("")
    const [model, setModel] = useState("")

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new car to inventory</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} required type="text" name="color" id="color" className="form-control" />
                            <label>Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={year} onChange={handleYearChange} required type="number" name="year" id="year" className="form-control" />
                            <label>Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={handleVinChange} required type="text" name="vin" id="vin" className="form-control" />
                            <label>VIN</label>
                        </div>
                        <div className="mb-3">
                            <select value={model} onChange={handleModelChange} required name="model" id="model" className="form-select">
                                <option value="">Select a model</option>
                                {props.models.map(model=> {
                                    return (
                                        <option value={model.id} key={model.id}>
                                            {model.manufacturer.name} {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewAutoToInventory
