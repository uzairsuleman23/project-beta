import { NavLink } from 'react-router-dom'
import React from 'react'

function InventoryList(props) {

    function checkIfSold(auto) {
        return auto.is_sold === false
    }
    const unsold_autos = props.autos.filter(checkIfSold)
    return (
        <div>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Vehicle Identification Number</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                    </tr>
                </thead>
                <tbody>
                    {unsold_autos.map(auto => {
                        return(
                            <tr key={auto.href}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.model.name}</td>
                            </tr>
                    )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default InventoryList
